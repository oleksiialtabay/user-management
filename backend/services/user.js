const mongoose = require("mongoose");
const UserSchema = mongoose.model("User");

module.exports = class UserService {
  async getUsers(query, { offset = 0, limit = 10 }) {
    const queryObject = query ? { $text: { $search: `\"${query}\"` } } : {};
    const users = await UserSchema.find(
     queryObject
    )
      .skip(offset)
      .limit(limit);
    const total = await UserSchema.count(queryObject);
    return { users, total };
  }

  async saveUser(user) {
    try {
      return await UserSchema.create(user);
    } catch (err) {
      throw { name: "ValidationError", message: err.message };
    }
  }

  async deleteUser(id) {
    try {
      return await UserSchema.deleteOne({ _id: id });
    } catch (err) {
      console.error(err);
    }
  }
};
