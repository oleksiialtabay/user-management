const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    name: { type: String, match: [/^[A-Z](?![A-Z])/], required: true },
    surname: { type: String, match: [/^[A-Z](?![A-Z])/], required: true },
    email: { type: String, match: [/\S+@\S+\.\S+/], required: true }
  },
  { autoCreate: true }
);

UserSchema.index({ name: "text", surname: "text" });

module.exports = mongoose.model("User", UserSchema);
