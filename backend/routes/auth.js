const express = require("express");
const router = express.Router();

const RegisteredUserService = require("../services/registeredUser");

const registeredUserService = new RegisteredUserService();

router.post("/register", async (req, res, next) => {
  try {
    res.send({
      token: await registeredUserService.register(
        req.body.email,
        req.body.password
      )
    });
  } catch (err) {
    res.status(400);
    res.send({ message: err.message });
  }
});

router.post("/login", async (req, res, next) => {
  try {
    res.send({
      token: await registeredUserService.getToken(
        req.body.email,
        req.body.password
      )
    });
  } catch (err) {
    res.status(400);
    res.send({ message: err.message });
  }
});

module.exports = router;
