import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { User } from './user.model';
import { switchMapTo } from 'rxjs/operators';
import { of } from 'rxjs/internal/observable/of';
import { UsersResponse } from './users-response.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private endpoint = `${environment.apiUrl}/users`;

  constructor(private httpClient: HttpClient) {}

  getUsers(
    query: string = '',
    pageIndex: number = 0,
    pageSize: number = 10
  ): Observable<UsersResponse> {
    const offset = pageSize * pageIndex;
    return this.httpClient.get<UsersResponse>(this.endpoint, {
      params: { query, offset: offset.toString(), limit: pageSize.toString() }
    });
  }

  deleteUser(userId: string): Observable<boolean> {
    return this.httpClient
      .request('delete', this.endpoint, {
        body: { _id: userId }
      })
      .pipe(switchMapTo(of(true)));
  }

  saveUser(user: User): Observable<User> {
    return this.httpClient.post<User>(this.endpoint, user);
  }
}
