import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { User } from './user.model';
import { auditTime, switchMap, takeUntil } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/internal/Subject';
import { UsersResponse } from './users-response.model';

export const timeBetweenSearchRequests = 1000;

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.scss']
})
export class UserTableComponent implements OnInit, OnDestroy {
  usersToDisplay: User[] = [];

  displayedColumns: string[] = ['name', 'surname', 'email', 'delete'];

  totalUsers: number;

  searchQuery: string;

  pageIndex: number;

  pageSize = 10;

  searchQueryEventEmitter = new EventEmitter<string>();

  destroy$ = new Subject<void>();

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.userService.getUsers().subscribe(this.processUserResponse);

    this.searchQueryEventEmitter
      .pipe(auditTime(timeBetweenSearchRequests))
      .pipe(
        switchMap(res => {
          this.pageIndex = 0;
          return this.userService.getUsers(res, this.pageIndex, this.pageSize);
        })
      )
      .pipe(takeUntil(this.destroy$))
      .subscribe(this.processUserResponse);
  }

  deleteUser(user: User) {
    this.userService
      .deleteUser(user._id)
      .pipe(
        switchMap(res => {
          this.pageIndex = 0;
          return this.userService.getUsers(this.searchQuery);
        })
      )
      .subscribe(this.processUserResponse);
  }

  createUser() {
    this.router.navigateByUrl('/create-user');
  }

  searchQueryChanged(newValue: string) {
    this.searchQueryEventEmitter.next(newValue);
  }

  pageChanged($event) {
    this.pageIndex = $event.pageIndex;
    this.userService
      .getUsers(this.searchQuery, this.pageIndex, this.pageSize)
      .subscribe(this.processUserResponse);
  }

  private processUserResponse = (res: UsersResponse) => {
    this.usersToDisplay = res.users;
    this.totalUsers = res.total;
  };

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
