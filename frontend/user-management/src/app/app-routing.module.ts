import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserTableComponent } from './user-table/user-table.component';
import { CreateUserComponent } from './user-table/create-user/create-user.component';
import { AuthComponent } from './auth/auth.component';
import { AuthGuardService } from './auth/auth.guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: AuthComponent },
  {
    path: 'users',
    component: UserTableComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'create-user',
    component: CreateUserComponent,
    canActivate: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
