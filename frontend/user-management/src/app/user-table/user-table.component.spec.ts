import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
  waitForAsync
} from '@angular/core/testing';

import {
  timeBetweenSearchRequests,
  UserTableComponent
} from './user-table.component';
import { Observable } from 'rxjs/internal/Observable';
import { UsersResponse } from './users-response.model';
import { of } from 'rxjs/internal/observable/of';
import { UserService } from './user.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatTableModule } from '@angular/material/table';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { MatInputModule } from '@angular/material/input';

describe('UserTableComponent', () => {
  let component: UserTableComponent;
  let fixture: ComponentFixture<UserTableComponent>;
  let userService: UserService;
  let loader: HarnessLoader;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        providers: [
          UserTableComponent,
          { provide: UserService, useClass: MockUserService }
        ],
        imports: [
          RouterTestingModule,
          MatTableModule,
          BrowserAnimationsModule,
          MatInputModule,
          BrowserModule
        ],
        schemas: [NO_ERRORS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    component = TestBed.inject(UserTableComponent);
    userService = TestBed.inject(UserService);
    fixture = TestBed.createComponent(UserTableComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create array with two items', async () => {
    component.ngOnInit();
    expect(component.usersToDisplay.length).toBe(2);
  });

  it('should display single item after search is complete', fakeAsync(() => {
    component.ngOnInit();
    component.searchQueryChanged('test2');
    fixture.detectChanges();
    tick(timeBetweenSearchRequests);
    expect(component.usersToDisplay.length).toBe(1);
  }));
});

class MockUserService {
  getUsers(
    query: string = '',
    pageIndex: number = 0,
    pageSize: number = 10
  ): Observable<UsersResponse> {
    const users = [
      {
        _id: 'test',
        name: 'John',
        surname: 'Johnatan',
        email: 'john@test.com'
      },
      {
        _id: 'test2',
        name: 'John2',
        surname: 'Johnatan2',
        email: 'john2@test.com'
      }
    ];
    if (query === 'test2') {
      return of({
        users: [users[1]],
        total: 1
      });
    } else {
      return of({
        users: users,
        total: 2
      });
    }
  }
}
