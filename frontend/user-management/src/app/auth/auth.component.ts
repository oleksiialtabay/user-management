import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

const validatorForEmail = /\S+@\S+\.\S+/;

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {
  authForm = this.fb.group({
    email: [
      '',
      Validators.compose([
        Validators.required,
        Validators.pattern(validatorForEmail)
      ])
    ],
    password: ['', Validators.compose([Validators.required])]
  });

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {}

  login() {
    if (this.authForm.valid) {
      this.authService.login(this.authForm.value).subscribe(res => {
        this.router.navigateByUrl('/users');
      });
    }
  }

  register() {
    if (this.authForm.valid) {
      this.authService.register(this.authForm.value).subscribe(res => {
        this.router.navigateByUrl('/users');
      });
    }
  }
}
