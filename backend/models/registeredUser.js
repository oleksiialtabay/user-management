const mongoose = require("mongoose");

const RegisteredUserSchema = new mongoose.Schema(
    {
        email: { type: String, match: [/\S+@\S+\.\S+/], required: true, unique:true },
        password: {type: String, required:true}
    },
    { autoCreate: true }
);


module.exports = mongoose.model("RegisteredUser", RegisteredUserSchema);