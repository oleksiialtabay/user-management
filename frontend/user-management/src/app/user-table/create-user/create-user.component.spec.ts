import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick
} from '@angular/core/testing';

import { CreateUserComponent } from './create-user.component';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';
import { User } from '../user.model';
import { MatTableModule } from '@angular/material/table';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { UserService } from '../user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';
import { Router } from '@angular/router';

describe('CreateUserComponent', () => {
  let component: CreateUserComponent;
  let fixture: ComponentFixture<CreateUserComponent>;

  let loader: HarnessLoader;
  let userService: UserService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        CreateUserComponent,
        { provide: UserService, useClass: MockUserService },
        { provide: Router, useClass: MockRouter }
      ],
      imports: [
        MatTableModule,
        BrowserAnimationsModule,
        MatInputModule,
        FormsModule,
        ReactiveFormsModule
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    component = TestBed.inject(CreateUserComponent);
    userService = TestBed.inject(UserService);
    fixture = TestBed.createComponent(CreateUserComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should  not create user with empty fields', fakeAsync(() => {
    let saveUserSpy = spyOn(userService, 'saveUser');
    component.createUser();
    tick();
    fixture.detectChanges();
    expect(saveUserSpy).not.toHaveBeenCalled();
  }));

  it('should  not create user with invalid fields', fakeAsync(() => {
    spyOn(userService, 'saveUser');
    component.userForm.setValue({
      name: 'INVALID',
      surname: 'Valid',
      email: 'john@test.com'
    });
    tick();
    component.createUser();
    fixture.detectChanges();
    expect(userService.saveUser).not.toHaveBeenCalled();
  }));

  it('should create user with valid fields', fakeAsync(() => {
    spyOn(userService, 'saveUser').and.returnValue(
      of({
        _id: 'test',
        name: 'John',
        surname: 'Johnatan',
        email: 'john@test.com'
      })
    );
    component.userForm.setValue({
      name: 'Valid',
      surname: 'Valid',
      email: 'john@test.com'
    });
    tick();
    component.createUser();
    fixture.detectChanges();
    expect(userService.saveUser).toHaveBeenCalled();
  }));
});

class MockUserService {
  saveUser(user: User): Observable<User> {
    return of({
      _id: 'test',
      name: 'John',
      surname: 'Johnatan',
      email: 'john@test.com'
    });
  }
}

class MockRouter {
  navigateByUrl() {}
}
