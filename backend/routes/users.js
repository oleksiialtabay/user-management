const express = require("express");
const router = express.Router();

const UserService = require("../services/User");
const RegisteredUserService = require("../services/registeredUser");

const userService = new UserService();
const registeredUserService = new RegisteredUserService();

router.get("/", async (req, res, next) => {
  try {
    await registeredUserService.validateTokenFromRequest(req);
    res.send(
      await userService.getUsers(req.query.query, {
        offset: parseInt(req.query.offset),
        limit: parseInt(req.query.limit)
      })
    );
  } catch (err) {
    next(err);
  }
});

router.post("/", async (req, res) => {
  await registeredUserService.validateTokenFromRequest(req);
  try {
    const user = await userService.saveUser(req.body);
    res.send(user);
  } catch (err) {
    if (err.name === "ValidationError") {
      res.status(400);
      res.send({ message: err.message });
    }
  }
});

router.delete("/", async (req, res) => {
  res.send(await userService.deleteUser(req.body._id));
});

module.exports = router;
