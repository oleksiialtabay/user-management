import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let headers = {};
    let token = this.authService.getToken();
    if (req.url.indexOf('auth') === -1) {
      if (token) {
        headers['Authorization'] = `Bearer ${token}`;
      }

      const request = req.clone({ setHeaders: headers });

      return next.handle(request);
    } else {
      return next.handle(req);
    }
  }
}
