const expect = require("chai").expect;

const User = require("../models/User");
describe("user", () => {
  it("should be invalid without name, surname or email", done => {
    let user = new User();
    user.validate(err => {
      expect(err.errors).to.have.all.keys(["name", "surname", "email"]);
      done();
    });
  });
  it("should be valid with user", done => {
    let user = new User({
      name: "John",
      surname: "Johnatan",
      email: "johnatan@test.com"
    });

    user.validate(err => {
      expect(err).to.be.null;
      done();
    });
  });

  it("should be invalid with a small first letter in the name", done => {
    let user = new User({
      name: "john",
      surname: "Johnatan",
      email: "johnatan@test.com"
    });
    user.validate(err => {
      expect(err.errors).to.have.all.keys(["name"]);
      done();
    });
  });

  it("should be invalid with the second capital letter in the name", done => {
    let user = new User({
      name: "JOhn",
      surname: "Johnatan",
      email: "johnatan@test.com"
    });
    user.validate(err => {
      expect(err.errors).to.have.all.keys(["name"]);
      done();
    });
  });

  it("should be invalid with invalid email", done => {
    let user = new User({
      name: "John",
      surname: "Johnatan",
      email: "johnatan@testcom"
    });
    user.validate(err => {
      expect(err.errors).to.have.all.keys(["email"]);
      done();
    });
  });
});

