import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { switchMapTo, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { of } from 'rxjs/internal/observable/of';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(private httpClient: HttpClient) {}

  register({ email, password }): Observable<boolean> {
    return this.httpClient
      .post(`${environment.apiUrl}/auth/register`, {
        email,
        password
      })
      .pipe(
        tap((res: { token }) => {
          this.saveToken(res.token);
        }),
        switchMapTo(of(true))
      );
  }

  login({ email, password }): Observable<boolean> {
    return this.httpClient
      .post(`${environment.apiUrl}/auth/login`, {
        email,
        password
      })
      .pipe(
        tap((res: { token }) => {
          this.saveToken(res.token);
        }),
        switchMapTo(of(true))
      );
  }

  saveToken(token: string): void {
    localStorage.setItem('token', token);
  }

  getToken(): string | null {
    return localStorage.getItem('token');
  }
}
