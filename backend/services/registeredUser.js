const mongoose = require("mongoose");
const RegisteredUserSchema = mongoose.model("RegisteredUser");
const jwt = require("jsonwebtoken");
const secret = require("../secret");

module.exports = class RegisteredUserService {
  async getUsers(query, { offset = 0, limit = 10 }) {
    const queryObject = query ? { $text: { $search: `\"${query}\"` } } : {};
    const users = await RegisteredUserSchema.find(queryObject)
      .skip(offset)
      .limit(limit);
    const total = await RegisteredUserSchema.count(queryObject);
    return { users, total };
  }

  async register(email, password) {
    try {
      const user = await RegisteredUserSchema.create({ email, password });
      return jwt.sign(user.toJSON(), secret);
    } catch (err) {
      throw { name: "ValidationError", message: err.message };
    }
  }

  async getRegisteredUser(email, password) {
    return await RegisteredUserSchema.findOne({
      email: email,
      password: password
    });
  }

  async getToken(email, password) {
    const user = await this.getRegisteredUser(email, password);
    if (user) {
      return jwt.sign(JSON.stringify(user), secret);
    } else {
      throw { name: "AuthenticationError", message: "Wrong email or password" };
    }
  }

  async validateTokenFromRequest(req) {
    const token = req.headers && req.headers.authorization;
    if (token) {
      return await this.validateToken(token.replace(/^Bearer\s/, ""));
    } else {
      throw { name: "UnauthorizedError", message: "You need to authorize" };
    }
  }

  async validateToken(token) {
    return await jwt.verify(token, secret);
  }
};
