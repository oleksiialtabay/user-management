import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

const validatorForName = /^[A-Z](?![A-Z])/;
const validatorForEmail = /\S+@\S+\.\S+/;

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  userForm = this.fb.group({
    name: ['', Validators.compose([Validators.required, Validators.pattern(validatorForName)])],
    surname: ['', Validators.compose([Validators.required, Validators.pattern(validatorForName)])],
    email: ['', Validators.compose([Validators.required, Validators.pattern(validatorForEmail)])]
  });

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {}

  createUser() {
    this.userForm.markAllAsTouched();
    if (this.userForm.valid) {
      this.userService.saveUser(this.userForm.value).subscribe(res => {
        this.router.navigateByUrl('/users');
      });
    }
  }
}
